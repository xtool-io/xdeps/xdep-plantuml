#!/bin/bash

mvn install:install-file -Dfile=plantuml-1.2023.6.jar \
  -DgroupId=net.sourceforge.plantuml \
  -DartifactId=plantuml \
  -Dversion=1.2023.6 \
  -Dpackaging=jar
