package xtool.xdepplantuml.command.provider

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import xtool.xdepplantuml.tasks.PlantTask
import java.nio.file.Paths

@Component
class PlantClassDiagramValueProvider(
    private val env: Environment,
    private val plantTask: PlantTask
) : Iterable<String> {
    override fun iterator(): Iterator<String> {
        val workspace = Paths.get(System.getProperty("user.dir"))
        val plantClassDiagramsPath = workspace.resolve(env.getRequiredProperty("plantuml.path"))
        return plantTask.listDiagrams(plantClassDiagramsPath)
            .map { workspace.relativize(it.path).toString() }
            .listIterator()

    }
}
