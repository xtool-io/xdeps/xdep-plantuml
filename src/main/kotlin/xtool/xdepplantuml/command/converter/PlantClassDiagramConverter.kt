package xtool.xdepplantuml.command.converter

import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import picocli.CommandLine.ITypeConverter
import xtool.xdepplantuml.model.PlantClassDiagram
import xtool.xdepplantuml.tasks.PlantTask
import java.nio.file.Paths

@Component
class PlantClassDiagramConverter(
    private val env: Environment,
    private val plantTask: PlantTask
) : ITypeConverter<PlantClassDiagram> {
    override fun convert(value: String?): PlantClassDiagram {
        val workspace = Paths.get(System.getProperty("user.dir"))
        val plantClassDiagramsPath = workspace.resolve(env.getRequiredProperty("plantuml.path"))
        return plantTask.listDiagrams(plantClassDiagramsPath)
            .find { workspace.relativize(it.path).toString() == value }!!
    }
}
