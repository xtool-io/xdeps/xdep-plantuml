package xtool.xdepplantuml

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
@ComponentScan
class XdepPlantuml {
    private val log = LoggerFactory.getLogger(XdepPlantuml::class.java)

    @PostConstruct
    fun init() {
        log.debug("xdep-plantuml carregado com sucesso.")
    }
}
