package xtool.xdepplantuml.utils

import net.sourceforge.plantuml.cucadiagram.Member

/**
 * Pattern de definição de atributo de uma classe
 */
private val fieldPattern = """-?\s*(\w+)\s*:\s*(\w+)\s*(\[\d*])?\s*(\{\w+\s*(?>,\s*\w+)*})?""".toRegex()

/**
 * Retorna o nome do atributo da classe.
 */
val Member.fieldName: String?
    get() {
        val matchResult: MatchResult? = fieldPattern.matchEntire(this.getDisplay(false))
        return matchResult?.groups?.get(1)?.value
    }

/**
 * Retorna o tipo do atributo da classe.
 */
val Member.fieldType: String?
    get() {
        val matchResult: MatchResult? = fieldPattern.matchEntire(this.getDisplay(false))
        return matchResult?.groups?.get(2)?.value
    }

/**
 * Retorna a multiplicidade do atributo
 */
val Member.fieldMultiplicity: String?
    get() {
        val matchResult: MatchResult? = fieldPattern.matchEntire(this.getDisplay(false))
        return matchResult?.groups?.get(3).let { it?.value?.removePrefix("[")?.removeSuffix("]") }
    }

/**
 * Retorna as propriedades do atributo.
 */
val Member.fieldProperties: List<String>?
    get() {
        val matchResult: MatchResult? = fieldPattern.matchEntire(this.getDisplay(false))
        return matchResult?.groups?.get(4).let { it?.value?.removePrefix("{")?.removeSuffix("}")?.split(",")?.map { item -> item.trim() } }
    }
