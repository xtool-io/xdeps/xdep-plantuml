package xtool.xdepplantuml.tasks

import net.sourceforge.plantuml.SourceStringReader
import net.sourceforge.plantuml.classdiagram.ClassDiagram
import org.springframework.stereotype.Component
import xtool.xdepplantuml.model.PlantClassDiagram
import xtool.xdepplantuml.model.PlantField
import xtool.xdepplantuml.model.PlantRelationship
import java.nio.file.Files
import java.nio.file.Path
import kotlin.streams.toList

/**
 * Classe com tarefas relacionadas a manipulação de arquivo *.plantuml
 */
@Component
class PlantTask {

    /**
     * Lê um diagrama de classe plantUML.
     * @return Objeto [PlantClassDiagram]
     */
    fun readClassDiagram(path: Path): PlantClassDiagram {
        require(!Files.notExists(path)) { "Diagrama de classe não encontrado em $path" }
        val diagram = String(Files.readAllBytes(path))
        val reader = SourceStringReader(diagram)
        return reader.blocks.asSequence()
            .map { it.diagram }
            .filter { it is ClassDiagram }
            .map { it as ClassDiagram }
            .map { PlantClassDiagram(path, it) }
            .first()
    }

    /**
     * Lê os diagramas de classe a partir do diretório base.
     * Os diagramas de classe devem possui a extensão *.class.puml ou *.class.plantuml
     * @return Lista de [PlantClassDiagram]
     */
    fun listDiagrams(basePath: Path): List<PlantClassDiagram> {
        return Files.list(basePath)
            .filter { Files.isRegularFile(it) }
            .filter { it.toFile().name.endsWith(".class.puml") || it.toFile().name.endsWith(".class.plantuml") }
            .map { readClassDiagram(it) }
            .toList()
    }

    /**
     * Retorna true caso o atributo [plantField] seja o id da entidade. Para ser id da entidade o atributo deve satisfazer uma
     * das condições a seguir:
     * <ol>
     *     <li>Possuir o nome id</li>
     *     <li>Possuir a propriedade id</li>
     * </ol>
     */
    fun isId(plantField: PlantField) = plantField.name == "id" || plantField.properties.contains("id")

    /**
     * Retorna true caso o atributo possua a propriedade [property].
     */
    fun containsProperty(plantField: PlantField, property: String) = plantField.properties.contains(property)

    /**
     * Retorna true caso o atributo [plantField] possua multiplicidade
     */
    fun hasMultiplicity(plantField: PlantField) = !plantField.multiplicity.isNullOrBlank()

//    fun isOneToMany(plantRelationship: PlantRelationship): Boolean {
//        if (listOf("*", "0..*", "1..*").contains(plantRelationship.multiplicities.first)) {
//
//        }
//        return false
//        //        return when {
////            listOf("*", "0..*", "1..*").contains(plantRelationship.multiplicities.first)
////            else -> false
////        }
//    }
}
