package xtool.xdepplantuml.model

import net.sourceforge.plantuml.abel.Entity
import net.sourceforge.plantuml.abel.Link
import net.sourceforge.plantuml.decoration.LinkDecor
import java.lang.RuntimeException

/**
 * Classe que representa um relacionamento no diagrama de classe
 */
class PlantRelationship(
    private val plantClassDiagram: PlantClassDiagram,
    val link: Link,
    val firstEntity: Entity,
    val secondEntity: Entity,
) {
    /**
     * Retorna true caso o relacionamento seja uma associação. Os
     */
    val isAssociation: Boolean
        get() = (listOf(LinkDecor.NONE, LinkDecor.ARROW).contains(link.type.decor1) && listOf(LinkDecor.NONE, LinkDecor.ARROW).contains(link.type.decor2))

    /**
     * Retorna true caso o relacionamento seja uma composição
     */
    val isComposition: Boolean
        get() = (link.type.decor1 == LinkDecor.COMPOSITION && listOf(LinkDecor.NONE, LinkDecor.ARROW).contains(link.type.decor2)) ||
                (listOf(LinkDecor.NONE, LinkDecor.ARROW).contains(link.type.decor1) && link.type.decor2 == LinkDecor.COMPOSITION)

    /**
     * Retorna true caso o relacionamento seja uma herança.
     */
    val isInheritance: Boolean
        get() = (link.type.decor1 == LinkDecor.EXTENDS && link.type.decor2 == LinkDecor.NONE) ||
                (link.type.decor1 == LinkDecor.NONE && link.type.decor2 == LinkDecor.EXTENDS)

    /**
     * Retorna true caso seja um auto-relacionamento.
     */
    val isSelfAssociation: Boolean
        get() = isAssociation && (secondEntity.name == firstEntity.name)

    /**
     * Retorna true se o relacionamento é unidirecional
     */
    val isUnidirectional: Boolean
        get() = (link.type.decor1 == LinkDecor.ARROW && link.type.decor2 == LinkDecor.NONE) ||
                (link.type.decor1 == LinkDecor.NONE && link.type.decor2 == LinkDecor.ARROW)


    /**
     * Retorna um objeto [Pair] com o par de classes do relacionamento.
     *
     *
     */
    val plantClasses: Pair<PlantClass, PlantClass>
        get() {
            val firstPlantClass =
                this.plantClassDiagram.classes.find { it.name == firstEntity.name } ?: throw RuntimeException("Erro ao buscar classe ${firstEntity.name}")
            val secondPlantClass =
                this.plantClassDiagram.classes.find { it.name == secondEntity.name } ?: throw RuntimeException("Erro ao buscar classe ${firstEntity.name}")
            return firstPlantClass to secondPlantClass
        }

    /**
     * Retorna um objeto [Pair] com o par de multiplicidade do relacionamento
     */
    val multiplicities: Pair<String, String>
        get() = when (link.isInverted) {
            true -> link.quantifier2 to link.quantifier1
            else -> link.quantifier1 to link.quantifier2
        }

    /**
     * Retorna a navegabilidade do relacionamento.
     */
    val navigabilities: Pair<Boolean, Boolean>
        get() {
            if (isAssociation) {
                if (link.type.decor1 == LinkDecor.ARROW && link.type.decor2 == LinkDecor.ARROW) return true to true
                if (link.isInverted && link.type.decor2 == LinkDecor.ARROW) return true to false
                if (link.type.decor1 == LinkDecor.ARROW) return true to false
                if (link.type.decor2 == LinkDecor.ARROW) return false to true
            }
            return false to false
        }


    override fun toString(): String {
        return """
            PlantRelationship(${plantClasses})
        """.trimIndent()
    }
}
