package xtool.xdepplantuml.model

import net.sourceforge.plantuml.cucadiagram.Member
import xtool.xdepplantuml.utils.fieldMultiplicity
import xtool.xdepplantuml.utils.fieldName
import xtool.xdepplantuml.utils.fieldProperties
import xtool.xdepplantuml.utils.fieldType
import java.lang.IllegalArgumentException

/**
 * Classe que representa um atributo de classe do diagrama de classe plantUML.
 */
class PlantField(
    private val plantClass: PlantClass,
    private val member: Member
) {

    /**
     * Retorna o nome do atributo da classe PlantUML
     */
    val name: String
        get() = this.member.fieldName ?: throw IllegalArgumentException("Não foi possível identificar o nome de algum atributo da classe ${plantClass.name}")

    /**
     * Retorna o tipo do atributo
     */
    val type: String
        get() = this.member.fieldType ?: throw IllegalArgumentException("Não foi possível identificar o tipo para o atributo $name da classe ${plantClass.name}")

    /**
     * Retorna a multiplicidade do atributo
     */
    val multiplicity: String?
        get() = this.member.fieldMultiplicity

    /**
     * Retorna as propriedades do atributo.
     */
    val properties: List<String>
        get() = this.member.fieldProperties ?: listOf()

}
