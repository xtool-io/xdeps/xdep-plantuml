package xtool.xdepplantuml.model

import net.sourceforge.plantuml.abel.Entity
import net.sourceforge.plantuml.cucadiagram.Member
import net.sourceforge.plantuml.skin.VisibilityModifier

/**
 * Classe que representa uma classe do diagrama PlantUML.
 */
data class PlantClass(
    val plantClassDiagram: PlantClassDiagram,
    val entity: Entity
) {

    /**
     * Retorna o nome da classe.
     */
    val name: String
        get() = entity.name

    /**
     * Retorna o nome do pacote no qual a classe pertence.
     */
    val packageName: String?
        get() = plantClassDiagram.classDiagram.entityFactory.groups().first()?.name;

    /**
     * Retorna a lista de atributos da classe.
     */
    val attributes by lazy {
        this.entity.bodier.fieldsToDisplay.asSequence()
            .filter { it.isNotEmpty() }
            .map { it as Member }
            .filter { it.visibilityModifier == VisibilityModifier.PRIVATE_FIELD }
            .map { PlantField(this, it) }
            .toList()
    }

    /**
     * Retorna a lista de estereotipos da classe.
     * Para maiores detalhes de como definir estereótipos no PlantUML ver [PlantUML Stereotypes](https://plantuml.com/class-diagram#59c91a18bcc97bb0){:target="_blank"}
     */
    val stereotypes: List<String>
        get() = this.entity.stereotype?.multipleLabels ?: listOf()


    override fun toString(): String {
        return """PlantClass(""" +
                "name='$name', " +
                "packageName=$packageName,\n" +
                "stereotypes=$stereotypes,\n" +
                ")"
    }


}
