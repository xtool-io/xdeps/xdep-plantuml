package xtool.xdepplantuml.model

import net.sourceforge.plantuml.abel.LeafType
import net.sourceforge.plantuml.classdiagram.ClassDiagram
import java.nio.file.Path
import kotlin.io.path.name
import kotlin.streams.toList


/**
 * Classe que representa um diagrama de classe PlantUML
 */
data class PlantClassDiagram internal constructor(
    val path: Path,
    val classDiagram: ClassDiagram
) {


    /**
     * Retorna o nome do diagram (com extensão .plantuml)
     */
    val name: String
        get() = this.path.name

    /**
     * Retorna a lista com as classes do diagrama
     */
    val classes by lazy {
        this.classDiagram.entityFactory.groups().stream()
            .flatMap { it.leafs().stream() }
            .filter { it.leafType == LeafType.CLASS }
            .map { PlantClass(this, it) }
            .toList()
    }

    /**
     * Retorna a lista com os relacionamentos do diagrama
     */
    val relationships by lazy {
        classDiagram.entityFactory.links.asSequence()
            .filter { !it.type.isInvisible }
            .filter { it.entity1.leafType == LeafType.CLASS }
            .filter { it.entity2.leafType == LeafType.CLASS }
            .map {
                // Caso algum definidor de layout seja definido no diagrama verifica se ele se toram "invertido" em
                // relação a definição que está no diagrama
                when (it.isInverted) {
                    true -> PlantRelationship(this, it, it.entity2, it.entity1)
                    else -> PlantRelationship(this, it, it.entity1, it.entity2)
                }
            }
            .toList()
    }

    /**
     * Retorna a lista com os Enuns do diagrama
     */
//    val enums: Set<PlantEnum>
//        get() = classDiagram.getGroups(false).stream()
//            .flatMap { it.leafsDirect.stream() }
//            .filter { it -> leaf.entityType == LeafType.ENUM }
//            .map { leaf: ILeaf? -> PlantEnum(leaf!!) }
//            .collect(Collectors.toSet())

    override fun toString(): String {
        return "PlantClassDiagram(path=$path, name='$name')"
    }


}
