package xdeps.xdepplantuml.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import xtool.xdepplantuml.tasks.PlantTask
import java.nio.file.Paths

class PlantRelationshipTestCase {

    @Test
    fun assert8AssociationsRelationships() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val associations = plantClassDiagram.relationships
            .filter { it.isAssociation }
        println(associations)
        Assertions.assertEquals(11, associations.size)
    }

    @Test
    fun assert1CompositionRelationships() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val compositions = plantClassDiagram.relationships
            .filter { it.isComposition }
        println(compositions)
        Assertions.assertEquals(1, compositions.size)
    }

    @Test
    fun assert1InheritanceRelationships() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val inheritances = plantClassDiagram.relationships
            .filter { it.isInheritance }
        println(inheritances)
        Assertions.assertEquals(1, inheritances.size)
    }

    @Test
    fun listRelationships() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val relationships = plantClassDiagram.relationships
        relationships.forEach {
//            println("ClassNames: ${it.plantClasses}")
            println("(${it.plantClasses.first.name}, ${it.plantClasses.second.name})")
            println("Multiplicities: ${it.multiplicities}")
            println("Navegabilities: ${it.navigabilities}")
            println("===")
        }
    }
}
