package xdeps.xdepplantuml.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import xtool.xdepplantuml.tasks.PlantTask
import java.nio.file.Paths

class PlantClassDiagramTestCase {

    @Test
    fun read10PlantClassesTest() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val classes = plantClassDiagram.classes
        Assertions.assertEquals(10, classes.size)
    }
}
