package xdeps.xdepplantuml

import net.sourceforge.plantuml.abel.LeafType
import net.sourceforge.plantuml.cucadiagram.Member
import net.sourceforge.plantuml.skin.VisibilityModifier
import org.junit.jupiter.api.Test
import xtool.xdepplantuml.tasks.PlantTask
import xtool.xdepplantuml.utils.fieldMultiplicity
import xtool.xdepplantuml.utils.fieldName
import xtool.xdepplantuml.utils.fieldProperties
import xtool.xdepplantuml.utils.fieldType
import java.nio.file.Paths

class PlantClassDiagramTestCase {

    @Test
    fun readClassDiagram() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
//        println(plantClassDiagram.classDiagram.entityFactory.groups().size)
//        plantClassDiagram.classes
//            .forEach { println(it.name) }

        plantClassDiagram.classes.asSequence()
            .flatMap { plantClass -> plantClass.entity.bodier.fieldsToDisplay.asSequence() }
            .map { it as Member }
            .filter { it.visibilityModifier == VisibilityModifier.PRIVATE_FIELD }
            .forEach { println("Nome: ${it.fieldName} Tipo: ${it.fieldType} Multiplicidade: ${it.fieldMultiplicity} Properties: ${it.fieldProperties}") }

    }

    @Test
    fun readRelationshipsDiagram() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
//        println(plantClassDiagram.classDiagram.entityFactory.groups().size)
        plantClassDiagram.classDiagram.entityFactory.links
            .filter { it.entity1.leafType == LeafType.CLASS }
            .filter { it.entity2.leafType == LeafType.CLASS }
//            .onEach { println("Is inverted: ${it.isInverted}") }
            .forEach {
                println("Left: ${it.entity1.name}, ${it.quantifier1}               Right: ${it.entity2.name} ${it.quantifier2}  ")
                println("Decor1: ${it.type.decor1}   Decor2: ${it.type.decor2}")
                println("Notas: ${it.note?.display}")
                println("===")
            }

    }

    @Test
    fun field1PatternTest() {
        val fieldPattern = """-\s*(\w+)\s*:\s*(\w+)\s*(\[\d*\])?\s*(\{\w+\s*(?>,\s*\w+)*\})?""".toRegex()
        val matchResult = fieldPattern.matchEntire("- nome: String[250] {notnull}")
        println(matchResult?.groups?.get(1)?.value)
    }

    @Test
    fun field2atternTest() {
        val fieldPattern = """-?\s*(\w+)\s*:\s*(\w+)\s*(\[\d*\])?\s*(\{\w+\s*(?>,\s*\w+)*\})?""".toRegex()
        val matchResult = fieldPattern.matchEntire("id: Long {id}")
        println(matchResult?.groups?.get(1)?.value)
    }

    @Test
    fun readStereotypesDiagram() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        plantClassDiagram.classes
            .forEach {
                println(it)
            }


    }

    @Test
    fun listRelationshipsDiagram() {
        val plantTask = PlantTask()
        val plantClassDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val relationships = plantClassDiagram.relationships
        println(relationships)

    }
}
