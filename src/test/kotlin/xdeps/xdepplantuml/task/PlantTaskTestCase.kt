package xdeps.xdepplantuml.task

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import xtool.xdepplantuml.tasks.PlantTask
import java.nio.file.Paths

class PlantTaskTestCase {

    @Test
    fun listDiagramsTest() {
        val plantTask = PlantTask()
        val listDiagrams = plantTask.listDiagrams(Paths.get("src/test/resources/src"))
        listDiagrams.forEach { println(it.name) }
        Assertions.assertEquals(2, listDiagrams.size)
    }

    @Test
    fun grauInstrucaoContainsNotNullAndUniquePropertiesTest() {
        val plantTask = PlantTask()
        val domainDiagram = plantTask.readClassDiagram(Paths.get("src/test/resources/domain.plantuml"))
        val grauIntrucaoClass = domainDiagram.classes.find { it.name == "GrauInstrucao" }
        val descricaoAttr = grauIntrucaoClass?.attributes?.find { it.name == "descricao" }
        println(descricaoAttr?.properties)
        Assertions.assertTrue(plantTask.containsProperty(descricaoAttr!!, "unique"))
        Assertions.assertTrue(plantTask.containsProperty(descricaoAttr!!, "notnull"))
        Assertions.assertEquals(2, descricaoAttr.properties.size)
    }
}
